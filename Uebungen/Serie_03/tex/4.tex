\section*{Aufgabe 4}

Wir bezeichnen mit $g$ die Funktion, die jeder natürlichen Zahl ihr
Quadrat zuordnet, und mit $\strikterw g$ ihre strikte Erweiterung, \ie
für alle $x\in\strikterw \bbn$ ist
\[
\strikterw g(x) =
\begin{cases}
  \bot & \text{falls $x=\bot$}\\
  x^2  & \text{sonst}\qquad.
\end{cases}
\]

Wir definieren das Funktional $\tau:\cpomont{\strikterw
  \bbn}{\strikterw \bbn}\to\cpomont{\strikterw
  \bbn}{\strikterw \bbn}$ für alle $f\in\cpomont{\strikterw
  \bbn}{\strikterw \bbn}$ und für alle $x\in\strikterw\bbn$ durch
\[
\tau[f](x) :=
\begin{cases}
  \bot & \text{falls $x=\bot$}\\
  0    & \text{falls $x=0$}\\
  2\,x-1+f(x-1) & \text{sonst}
\end{cases}
\]

und behaupten, dass $\strikterw g$ der kleinste Fixpunkt von $\tau$
ist.


Um dies zu beweisen, zeigen wir Folgendes:
\begin{enumerate}
\item Es gehört $\strikterw g$ zu $\cpomont{\strikterw
    \bbn}{\strikterw \bbn}$.
\item Es ist $\strikterw g$ ein Fixpunkt von $\tau$.
\item Für jeden Fixpunkt $h$ von $\tau$ gilt $\strikterw g \leqs
  h$.
\end{enumerate}
\hrulefill
\begin{enumerate}
\item Dass $\strikterw g$ der Menge $({\strikterw
    \bbn})^{(\strikterw \bbn)}$ angehört, ist klar.

  Es ist zu zeigen, dass $\strikterw g$ eine monotone Funktion
  ist. Seien dazu $x,\,y\in\strikterw \bbn$, und es gelte $x\leqs
  y$. Je nachdem  $x=\bot$ gilt oder nicht, unterscheiden wir zwei Fälle.% \footnote{Den
  % zu~\ref{kleiner} symmetrischen Fall $y=\bot \land x\in\strikterw \bbn$
  % betrachten wir nicht, weil er, sofern $x\neq y$ ist, die
  % Voraussetzung $x\leqs y$ nicht erfüllt und, sofern $x=y$ gilt, im
  % Fall~\ref{gleich} enthalten ist.}
  \begin{enumerate}
  \item $x\neq\bot$. Dann gehören insbesondere $x$ und $y$ beide zu $\bbn$, und es folgt \[\strikterw g(x) = x^2 \leqs y^2 =
    \strikterw g(y)\quad,\] wobei wir die Monotonie der Quadratfunktion
    benutzt haben.
  \item \label{kleiner} $x=\bot$. Dann gilt
    \[\strikterw g(x) = \strikterw g(\bot) = \bot \leqs y^2 =\strikterw
    g(y)\quad,\] wobei wir verwendet haben, dass $\bot$ ein kleinstes Element
    von $\strikterw \bbn$ ist.
  \end{enumerate}

  Damit wissen wir, dass $g$ monoton ist.

\item Um zu zeigen, dass $\strikterw g$ ein Fixpunkt von $\tau$ ist,
  ist $\tau\left[\strikterw g\right] = \strikterw g$ nachzuweisen, was
  mit
  \[
  \forall\ x\in\strikterw\bbn\quad \left(\tau[\strikterw
    g]\right)(x) = \strikterw g(x)
  \]
  gleichbedeutend ist.
  
  Zunächst gilt für alle $x\in\strikterw\bbn$:
  \[
  \tau[\strikterw g](x) :=
  \begin{cases}
    \bot & \text{falls $x=\bot$}\\
    0    & \text{falls $x=0$}\\
    2\,x-1+\strikterw g(x-1) & \text{sonst}
  \end{cases}
  \]

  Sei also $x\in\strikterw\bbn$.\\[0mm]

  \begin{minipage}[l]{0.47\linewidth}
    Falls $x=\bot$, so folgt
    \[
    \begin{array}{rclll}
      \strikterw g(x)&=&\strikterw g(\bot) & \text{}\\
                     &=&\bot & \text{Def. $\strikterw g$}\\
                     &=& \left(\tau[\strikterw g]\right)(\bot) &
                                                                  \text{Def. $\tau$}\\
                     &=& \left(\tau[\strikterw g]\right)(x)& \text{}\quad.\\
    \end{array}
    \]

  \end{minipage}
  \hspace{10mm}
  \begin{minipage}[r]{0.47\linewidth}
    
    Falls $x=0$, so folgt
    \[
    \begin{array}{rclll}
      \strikterw g(x) &=& \strikterw g(0) & \text{}\\
                      &=& 0 & \text{Def. $\strikterw g$}\\
                      &=& \left(\tau[\strikterw g]\right)(0) &
                                                                \text{Def. $\tau$}\\
                      &=& \left(\tau[\strikterw g]\right)(x)& \text{}\quad.\\
    \end{array}
    \]
  \end{minipage}

\vspace{5mm}

Falls $x\in\bbn_{>0}$ gilt, so führt eine Induktion zum Ziel:
Für alle $n\in\bbn$ zeigen wir die Aussage $\left(\tau[\strikterw
  g]\right)(n)=\strikterw g(n)$. Der Induktionsanfang steht
dann direkt oben rechts drüber. Für den Induktionsschritt sei
$n\in\bbn$, und es gelte $\left(\tau[\strikterw
  g]\right)(n)=\strikterw g(n)$. Dann folgt
\[
\begin{array}{rclll}
  \left(\tau[\strikterw g]\right)(n+1) &=& 2\,(n+1)-1+\strikterw g(n)&
                                                                     \text{Def. $\tau$}\\
  &=& 2\,n+1 + \strikterw g(n)& \text{}\\
  &=& 2\,n+1 + n^2& \text{Def. $\strikterw g$}\\
  &=& (n+1)^2& \text{}\\
  &=& \strikterw g(n+1)& \text{Def. $\strikterw g$}
\end{array}
\]

und damit die Aussage $\left(\tau[\strikterw
    g]\right)(x) = \strikterw g(x)$ für alle
  $x\in\strikterw\bbn$. Also ist $\strikterw g$ ein Fixpunkt von
  $\tau$.

\item Es bleibt zu zeigen, dass $\strikterw g$ der kleinste Fixpunkt von
  $\tau$ ist. Sei dazu $h$ ein Fixpunkt von $\tau$, dann íst
  $\strikterw g \leqs h$ zu zeigen. Wir rechnen $\strikterw g(n) \leqs
  h(n)$ für alle $n\in\strikterw\bbn$ nach:

  Es gilt $\strikterw g(\bot)=\bot\leqs\bot=h(\bot)$, wobei wir die
  Gleichheit $h(\bot)=\bot$ aus
  $h(\bot)=\left(\tau[h]\right)(\bot)=\bot$ erhalten, die besteht,
  weil $h$ ein Fixpunkt von $\tau$ ist.

  Ähnlich erhalten wir
  \[
  \begin{array}{rclll}
    \strikterw g(0) &=& 0 && \text{Def. $\strikterw g$}\\
                    &\leqs& 0\\
                    &=&\left(\tau[h]\right)(0) && \text{Def. $\tau$}\\
                    &=& h(0) && \text{$h$ ist Fixpunkt von $\tau$}
  \end{array}
  \]
  
  und führen für die verbleibenden Fälle erneut eine Induktion durch,
  die wir bei $n=0$ soeben verankert haben.

  Sei also $n\in\bbn$, und es gelte $\strikterw g(n) \leqs h(n)$. Dann
  folgt
  \[
  \begin{array}{rclll}
    \strikterw g(n+1) &=&(n+1)^2 && \text{Def. $\strikterw g$}\\
                      &=&2\,n+1+n^2\\
                      &=&2\,n+1+\strikterw g(n)&&
                                                  \text{Def. $\strikterw
                                                  g$}\\
                      &\leqs& 2\,n+1+h(n)&&
                                                  \text{nach IV}\\
                      &=& 2\,(n+1)-1+h(n)\\
                      &=& \left(\tau[h]\right)(n+1) &&
                                                       \text{Def. $\tau$}\\
                      &=& h(n+1) &&
                                                       \text{$h$ ist Fixpunkt von $\tau$}
  \end{array}
  \]

und es ist alles gezeigt. \fertich
\end{enumerate}
