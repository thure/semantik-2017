{
   http://rextester.com/l/pascal_online_compiler
    fpc 3.0.0
}

{
   fpc:
   fpc 2.pas
}

program P (input, output);

USES Crt; {wird beim online-Compiler nicht benoetigt}

VAR
   b : boolean;

function F : integer;
    begin
       b := true;
       F := 2;
    end;
function G : integer;
    begin
      if b
         then G:=3
         else G:=4
    end;

begin

   WriteLn('erste Reihenfolge:');

   b := false;   
   Write('F+G wird ausgewertet zu ');
   WriteLn(F+G);
   
   b := false;
   Write('G+F wird ausgewertet zu ');
   WriteLn(G+F);
   
   WriteLn;
   WriteLn('zweite Reihenfolge:');
   
   b := false;
   Write('G+F wird ausgewertet zu ');
   WriteLn(G+F);
   
   b := false;
   Write('F+G wird ausgewertet zu ');
   WriteLn(F+G);
   
end.
